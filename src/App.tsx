// import { useState } from 'react'
import { useCallback, useEffect, useMemo, useState } from "react";
import "./App.css";
import { Loading } from "./loading";

type TimingsType = {
  Asr: string;
  Dhuhr: string;
  Fajr: string;
  Firstthird: string;
  Imsak: string;
  Isha: string;
  Lastthird: string;
  Maghrib: string;
  Midnight: string;
  Sunrise: string;
  Sunset: string;
};

type ApiResType = {
  code: number;
  data: {
    timings: TimingsType;
  };
  status: string;
};

function App() {
  const date = useMemo(() => new Date(), []);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [data, setData] = useState<TimingsType>({
    Asr: "",
    Dhuhr: "",
    Fajr: "",
    Firstthird: "",
    Imsak: "",
    Isha: "",
    Lastthird: "",
    Maghrib: "",
    Midnight: "",
    Sunrise: "",
    Sunset: "",
  });
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [geoData, setGeoData] = useState<any>({});
  const getPrayerTime = useCallback(
    async (lat: number, lon: number) => {
      setLoading(true);
      const geoRes = await fetch(
        `https://api.opencagedata.com/geocode/v1/json?q=${lat}%2C${lon}&key=${
          import.meta.env.VITE_API_KEY
        }`
      ).then((geoData) => geoData.json());
      setGeoData(geoRes);
      const apiRes: ApiResType = await fetch(
        `https://api.aladhan.com/v1/timings/${date.getDate()}-${date.getMonth()}-${date.getFullYear()}?latitude=${lat}&longitude=${lon}`
      ).then((res) => res.json());
      if (apiRes.code === 200) {
        setData(apiRes.data.timings);
        setLoading(false);
      } else {
        setLoading(false);
        setError("Geolocation is not available");
      }
    },
    [date]
  );
  const getPosition = useCallback(() => {
    setLoading(true);
    const success = (pos: GeolocationPosition) => {
      if (pos.coords.latitude && pos.coords.longitude) {
        setLoading(false);
        getPrayerTime(pos.coords.latitude, pos.coords.longitude);
      }
    };
    const error = () => {
      setLoading(false);
      setError("Geolocation is not available");
    };
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(success, error);
    }
  }, [getPrayerTime]);

  useEffect(() => {
    getPosition();
  }, [getPosition]);

  return (
    <main>
      <div className="outer">
        <h1>Today's Prayer Time</h1>
        <h2>
          {geoData?.results?.length ? geoData?.results[0]?.formatted : null}
        </h2>
        {error.length ? (
          <h2>{error}</h2>
        ) : loading ? (
          <Loading />
        ) : data.Asr.length ? (
          <div>
            <h3>Fajr: {data.Fajr}</h3>
            <h3>Dhuhr: {data.Dhuhr}</h3>
            <h3>Asr: {data.Asr}</h3>
            <h3>Maghrib: {data.Maghrib}</h3>
            <h3>Isha: {data.Isha}</h3>
          </div>
        ) : null}
      </div>
    </main>
  );
}

export default App;
